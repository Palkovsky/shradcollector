@extends('layouts.default')

@section('title')
    Informacje
@endsection

@section('content')
	<div class="w3-container w3-center w3-blue">
		<h3>Logowanie/Rejestracja</h3>
	</div>
	<div class="w3-container w3-center">
		{!! Form::open(array('action' => 'SessionsController@dummy', 'method' => 'post')) !!}
			{!! Form::submit('Szybkie konto', array('class' => 'w3-btn w3-blue',
			'style' => 'margin-top: 10px;')) !!}
		{!! Form::close() !!}
		<p>Szybkie konto nie wymaga podawania żadnych danych.<br/> Zostanie ono przypisane do Twojej obecnej przeglądarki. Nie będziesz mógł się na nie zalogować z innej.</p>
		<span class="w3-text-grey w3-small">Upewnij się, że nie przeglądasz w trybie prywatnym.</span><br />
	<br/>

	<br />
		<div class="w3-container w3-center w3-large w3-animate-opacity" style="margin-bottom: 30px;">
			<ul>
			    @foreach($errors->all() as $error)
			        <li>{{ $error }}</li>
			    @endforeach
			</ul>

			{!! Form::open(array('action' => 'SessionsController@store', 'method' => 'post')) !!}
				{!! Form::label('email', 'Email: ') !!}
				{!! Form::text('email') !!}<br/>
				{!! Form::label('password', 'Hasło: ') !!}
				{!! Form::password('hasło') !!}<br/>
				{!! Form::submit('Zaloguj!', array('class' => 'w3-btn w3-blue',
				'style' => 'margin-top: 10px;')) !!}
			{!! Form::close() !!}
			<a href="{{ $login_url }}" class="w3-btn w3-blue w3-animate-top" style="margin-top:10px;">Zaloguj za pomocą Facebook'a</a>
		</div>

		<p>Powyższe metody umożliwiają ponowne zalogowanie się na jakimkolwiek urządzeniu czy przeglądarce.</p>
	</div>
@endsection