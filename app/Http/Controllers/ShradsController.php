<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Auth;
use Laracasts\Flash\Flash;

use App\Http\Requests;
use App\Shrad;

class ShradsController extends Controller
{
    public function index(){
        $shrads = Shrad::all();
        $pending_shrads = $shrads->count() - Auth::user()->shrads()->count();
        $percentage = intval((100 * Auth::user()->shrads()->count()) / $shrads->count());
    	$show_modal = !Auth::user()->informed && $percentage == 100;
        if($percentage == 100){
            Auth::user()->informed = true;
            Auth::user()->save();
        }
        return view('shrads/index', ["shrads" => $shrads, "percentage" => $percentage, "pending_shrads" => $pending_shrads, "show_modal" => $show_modal]);
    }

    public function show($slug){
    	$shrad = Shrad::where('slug' , '=', $slug)->first();
    	if($shrad && Auth::user()->isOwner($shrad)){
    		return view('shrads/show', ["shrad" => $shrad]);
    	}
    	Flash::message("Nie znaleziono.");
    	return Redirect::action('ShradsController@index');
    }

    public function present(Request $request){
        $uuid = $request->input('uuid');
        $shrad = Shrad::where('uuid' , '=', $uuid)->first();
        if($shrad && !Auth::user()->isOwner($shrad)){
            return view('shrads/present', ["shrad" => $shrad]);
        }else if($shrad && Auth::user()->isOwner($shrad)){
            return Redirect::action('ShradsController@show', ["slug" => $shrad->slug]);
        }else{
            return Redirect::action('ShradsController@index');
        }
    }

    //3f6bf701-6d17-41af-a213-f64a70e0f58f

    public function collect(Request $request){
        $uuid = $request->input('uuid');
        $shrad = Shrad::where('uuid' , '=', $uuid)->first();
        if($shrad && !Auth::user()->isOwner($shrad)){
            Auth::user()->shrads()->attach($shrad->id);
            return view('shrads/collect', ["shrad" => $shrad]);
            //return Redirect::action('ShradsController@show', ["slug" => $shrad->slug, "ref" => "present"]);
        }else if($shrad && Auth::user()->isOwner($shrad)){
            return Redirect::action('ShradsController@show', ["slug" => $shrad->slug]);
        }else{
            return Redirect::action('ShradsController@index');
        }
    }
}
