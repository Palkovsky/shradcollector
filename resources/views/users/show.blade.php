@extends('layouts.default')

@section('title')
	Rejestracja
@endsection


@section('content')
	<h1>Gratulacje</h1>
	<p>Twoje imię przypsiane przez skrypt to:</p>
	<h2>{{ $user->name }}</h2>
	<p>Do logowania używaj wskazane hasło oraz e-maila: <strong>{{ $user->email }}</strong></p>
	<p>Trochę słabo, że nie zdecydowałeś/łaś zalogować się przez Facebook'a.</p>
	<p>Ale spokojnie, to imię też jest unikatowie, więc będzie można się za jego pomocą zgłosić po nagrodę.</p>

	<div class="w3-container w3-center w3-large w3-animate-opacity" style="margin-top: 70px; margin-bottom: 50px;">
		<a href="{{ action('ShradsController@index') }}" class="w3-btn w3-blue w3-animate-top">Zobacz swoją układankę</a>
	</div>
@endsection