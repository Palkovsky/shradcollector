<?php

use Illuminate\Database\Seeder;
use App\User;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->delete();

        User::create(array(
        		'name' => 'administrator',
        		'email' => 'xxx909@interia.pl',
        		'password' => bcrypt('administrator')
        	));
    }
}
