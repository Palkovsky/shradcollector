<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="Keywords" content="">
        <meta name="Description" content="">
        <link rel="shortcut icon" href="{{ asset('favicon.png') }}" type="image/png">
        <meta name="theme-color" content="#2892ef" />
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
        <meta name="csrf_token" content="{{ csrf_token() }}">
        <link rel="stylesheet" href="{!! asset('css/w3.css') !!}">
        <link rel="stylesheet" href="{!! asset('css/present.css') !!}">

        @yield('links')
        <title>@yield('title')</title>
    </head>
    <body>
        @yield('after-body')
        @yield('content')
    </body>
</html>