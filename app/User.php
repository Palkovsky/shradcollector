<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public static function createOrUpdateGraphNode($graph_node){
        $graph_node = $graph_node->asArray();
        $user = User::where('facebook_id' , '=', $graph_node['id'])->first();

        if(!$user){
            $user = new User;
        }

        $user->facebook_id = $graph_node['id'];
        $user->name = $graph_node['name'];
        $user->email = $graph_node['email'];

        $user->save();

        return $user;
    }

    public function shrads(){
        return $this->belongsToMany('App\Shrad')->withTimestamps();
    }

    public function isOwner($shrad){
        return ($this->shrads()->get()->contains($shrad));
    }

    public function score(){
        return $this->shrads()->get()->count();
    }
}
