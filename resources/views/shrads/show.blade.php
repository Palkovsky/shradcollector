@extends('layouts.default')

@section('title')
	{{ $shrad->name }}
@endsection

@section('content')
	<div class="w3-container w3-center w3-blue">
		<h3>{{ $shrad->name }}</h3>
	</div>
	<div id="container"></div>
	<div class="w3-container w3-center w3-animate-zoom dynamic-margin-top">
		<div class="yt-video" id="player"/>
	</div>
	<div class="w3-container w3-center w3-large w3-animate-opacity" style="margin-top: 70px; margin-bottom: 50px;">
		<a href="{{ action('ShradsController@index') }}" class="w3-btn w3-blue w3-animate-top">Zobacz swoją układankę</a>
	</div>


	<script src="{!! asset('js/progressbar.min.js') !!}"></script>
	<script src="https://www.youtube.com/iframe_api"></script>
	<script type="text/javascript">
		 var player, playing = false;
		 function onYouTubeIframeAPIReady() {
		    player = new YT.Player('player', {
		        height: '250',
		        width: '444',
		        videoId: "<?php echo($shrad->video_url); ?>",
		       	playerVars: { 
				    'autoplay': 1,
				    'controls': 1, 
				    'rel' : 0
				},
		        events: {
		       		'onStateChange': onPlayerStateChange
		     	}
		   	});
		 }


		 var interval; 

		function onPlayerStateChange(event) {
			console.log('state change');
			if(event.data == YT.PlayerState.PLAYING){
				if(!interval){
					interval = window.setInterval(function(){
							updateBar(player.getCurrentTime(), player.getDuration());
					}, 300);
				}
			}
		}

		var bar = new ProgressBar.Line('#container', {
		  strokeWidth: 4,
		  easing: 'easeInOut',
		  duration: 0,
		  color: '#FFEA82',
		  trailColor: '#eee',
		  trailWidth: 0,
		  svgStyle: {width: '100%', height: '100%'}
		});
		bar.animate(0);  // Value from 0.0 to 1.0

		function updateBar(time, duration){
			var progress = (time * 100) / duration;
			progress = progress/100;
			bar.animate(progress);
		}


	</script>
@endsection