@extends('layouts.default')

@section('title')
    Informacje
@endsection

@section('content')
    <h1>Strona powitalna</h1>
    <a href="{{ action('UsersController@create') }}">Stwórz konto</a><br/>
    <a href="{{ action('SessionsController@create') }}">Zaloguj się</a><br/>
	<a href="{{ $login_url }}">Zaloguj się przez Facebooka</a>
@endsection