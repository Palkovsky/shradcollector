@extends('layouts.default')


@section('title')
    {{$shrad->name}}
@endsection

@section('content')
	<div class="w3-container w3-center w3-blue">
		<h3>Oto kolejny element</h3>
	</div>
	<div class="w3-container w3-center w3-animate-zoom" style="margin-top: 70px;">
		<img src="{{ $shrad->image_url }}" class="w3-animate-opacity"/>
	</div>
	<div class="w3-container w3-center w3-large w3-animate-opacity" style="margin-top: 70px; margin-bottom: 50px;">
		<a href="{{ action('ShradsController@index') }}" class="w3-btn w3-blue w3-animate-top">Zobacz swoją układankę</a>
	</div>
@endsection