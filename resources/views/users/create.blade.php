@extends('layouts.default')

@section('title')
	Rejestracja
@endsection


@section('content')
	<h1>Users#Create</h1>
	<ul>
	    @foreach($errors->all() as $error)
	        <li>{{ $error }}</li>
	    @endforeach
	</ul>
	{!! Form::open(array('action' => 'UsersController@store', 'method' => 'post')) !!}
	    {!! Form::label('email', 'Email: ') !!}
	    {!! Form::text('email') !!}<br/>
	    {!! Form::label('password', 'Hasło: ') !!}
	    {!! Form::password('password') !!}<br/>
	    {{ Form::label('gender', 'Płeć: ', array('class' => 'control-label')) }}
	    <label class="radio">
	  		{{ Form::radio('gender', 'Mężczyzna') }} Male
	    </label>

	  	<label class="radio">
	  		{{ Form::radio('gender', 'Kobieta') }} Female
	  	</label>
	  	<br/>
	 	{!! Form::submit('Zarejestruj!') !!}
	{!! Form::close() !!}
@endsection