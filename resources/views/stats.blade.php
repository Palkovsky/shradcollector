@extends('layouts.default')

@section('title')
    Ranking
@endsection

@section('content')
    <h1>Ranking</h1>

    <table border="1">
    	<thead>
    		<tr>
    			<td>Id</td>
    			<td>Nazwa</td>
    			<td>Email</td>
    			<td>Zebrano</td>
    		</tr>
    	</thead>
	    @foreach($users as $index => $user)
	    	<tbody>
		    	<tr>
		    		<td>{{ $user->id }}</td>
		    		<td>{{ $user->name }}</td>
		    		<td>{{ $user->email }}</td>
		    		<td>{{ $user->shrads()->count() }} z {{ $shrads->count() }}</td>
		    	</tr>
	    	</tbody>
		@endforeach
	</table>
@endsection