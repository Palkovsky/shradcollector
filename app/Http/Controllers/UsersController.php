<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Session;
use Laracasts\Flash\Flash;
use SammyK\LaravelFacebookSdk\LaravelFacebookSdk;
use App\Http\Requests;

use Faker\Factory as Faker;

use App\User;

class UsersController extends Controller
{
    public function create(){
    	if(Auth::user())
    		return Redirect::action('ShradsController@index');

    	return view('users/create');
    }

    public function show(){
    	if(!Auth::user())
    		return Redirect::action('UsersController@create');
    	return view('users/show', ['user' => Auth::user()]);
    }

    public function store(){
    	if(Auth::user())
    		return Redirect::action('ShradsController@index');
        $rules = array(
            'email' => 'required|email|unique:users',
            'gender' => 'required',
            'password' => 'required'
        );
        $validator = Validator::make(Input::all(), $rules);


        if ($validator->fails()) {
            return Redirect::action('UsersController@create')
                ->withErrors($validator)
                ->withInput(Input::except('password'));
        }else{
        	$user = new User;

        	$faker = Faker::create(); 
        	$gender = Input::get('gender');
        	if($gender == "Mężczyzna")
        		$user->name = $faker->firstNameMale." ".$faker->lastName;
        	else
        		$user->name = $faker->firstNameFemale." ".$faker->lastName;

        	$user->email = Input::get('email');
        	$user->password = bcrypt(Input::get('password'));
        	$user->save();

			Auth::login($user);  
        	return view('users/show', ['user' => $user]);
        }
    }



    public function facebook(){
    	$fb = app(LaravelFacebookSdk::class);
	    try {
	        $token = $fb->getAccessTokenFromRedirect();
	    } catch (Facebook\Exceptions\FacebookSDKException $e) {
	        dd($e->getMessage());
	    }

		if (! $token) {
	        // Get the redirect helper
	        $helper = $fb->getRedirectLoginHelper();

	        if (! $helper->getError()) {
	            abort(403, 'Unauthorized action.');
	        }

	        // User denied the request
	        dd(
	            $helper->getError(),
	            $helper->getErrorCode(),
	            $helper->getErrorReason(),
	            $helper->getErrorDescription()
	        );
	    }

		if (! $token->isLongLived()) {
	        // OAuth 2.0 client handler
	        $oauth_client = $fb->getOAuth2Client();

	        // Extend the access token.
	        try {
	            $token = $oauth_client->getLongLivedAccessToken($token);
	        } catch (Facebook\Exceptions\FacebookSDKException $e) {
	            dd($e->getMessage());
	        }
	    }

	    $fb->setDefaultAccessToken($token);

	    // Save for later
	    Session::put('fb_user_access_token', (string) $token);

		try {
	        $response = $fb->get('/me?fields=id,name,email');
	    } catch (Facebook\Exceptions\FacebookSDKException $e) {
	        dd($e->getMessage());
	    }

	    // Convert the response to a `Facebook/GraphNodes/GraphUser` collection
    	$facebook_user = $response->getGraphUser();

    	$user = User::createOrUpdateGraphNode($facebook_user);
    	Auth::login($user);    	

    	Flash::message("Zalogowany!");
    	return Redirect::intended('/');
    }
}
