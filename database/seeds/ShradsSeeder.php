<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\URL;
use App\Shrad;

class ShradsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('shrads')->delete();

        Shrad::create(array(
        		'name' => 'Kibel 1F',
        		'image_url' => URL::asset('shrad/1.png'),
        		'video_url' => 's5fUwjdeSHE',
                'content' => ''
        ));

        Shrad::create(array(
        		'name' => 'Kibel 2F',
        		'image_url' => URL::asset('shrad/2.png'),
        		'video_url' => 'B6LTT4RA-4o',
                'content' => ''
        ));

        Shrad::create(array(
        		'name' => 'Sala 26',
        		'image_url' => URL::asset('shrad/3.png'),
        		'video_url' => 'Gin-l4LDdXQ',
                'content' => ''
        ));

        Shrad::create(array(
        		'name' => 'Sala 35',
        		'image_url' => URL::asset('shrad/4.png'),
        		'video_url' => 'ItCa8KYj8FM',
                'content' => ''
        ));

        Shrad::create(array(
        		'name' => 'Parking',
        		'image_url' => URL::asset('shrad/5.png'),
        		'video_url' => 'iLkK_GIc_Nk',
                'content' => ''
        ));

        Shrad::create(array(
        		'name' => 'Sala gimnastyczna',
        		'image_url' => URL::asset('shrad/6.png'),
        		'video_url' => 'eNaYn7Xkovg',
                'content' => ''
        ));
    }
}
