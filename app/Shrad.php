<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\SluggableInterface;
use Cviebrock\EloquentSluggable\SluggableTrait;
use Webpatser\Uuid\Uuid;

class Shrad extends Model implements SluggableInterface
{
	use SluggableTrait;

	protected $table = 'shrads';
    public $timestamps = false;


    protected $sluggable = [
        'build_from' => 'name',
        'save_to'    => 'slug',
    ];

    public function save(array $options = array()){
        $this->uuid = Uuid::generate(4)->string;
        parent::save();
    }

    public function users(){
    	return $this->belongsToMany('App\User')->withTimestamps();
    }

    public function isOwned($user){
        return ($this->users()->get()->contains($user));
    }
}
