@extends('layouts.default')

@section('links')
@endsection

@section('title')
    {{$shrad->name}}
@endsection

@section('content')
	<div class="w3-container w3-center w3-blue">
		<h3>Zobacz film i zdobądź element</h3>
	</div>
	<div id="container"></div>
	<div class="w3-container w3-center w3-animate-zoom dynamic-margin-top">
		<div class="yt-video" id="player"/>
	</div>
	<div class="w3-container w3-center w3-small w3-text-grey">
		<p>Aby zdobyć element, musisz obejrzeć cały film</p>
	</div>


	<script src="{!! asset('js/progressbar.min.js') !!}"></script>
	<script src="https://www.youtube.com/iframe_api"></script>
		<script type="text/javascript">
		 var player, playing = false;
		    function onYouTubeIframeAPIReady() {
		      player = new YT.Player('player', {
		        height: '250',
		        width: '444',
		        videoId: "<?php echo($shrad->video_url); ?>",
		        playerVars: { 
				    'autoplay': 1,
				    'controls': 0, 
				    'rel' : 0
				},
		        events: {
		       		'onStateChange': onPlayerStateChange
		     	}
		   });
		 }

		 function getParameterByName(name, url) {
		    if (!url) url = window.location.href;
		    name = name.replace(/[\[\]]/g, "\\$&");
		    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
		        results = regex.exec(url);
		    if (!results) return null;
		    if (!results[2]) return '';
		    return decodeURIComponent(results[2].replace(/\+/g, " "));
		}

		var interval; 

			function onPlayerStateChange(event) {
				if(event.data == YT.PlayerState.ENDED){
					var url = '/collect';
					var uuid = '<?php echo($shrad->uuid) ?>';
					var csrf = $('meta[name=csrf_token]').attr('content');
					var form = $('<form action="' + url + '" method="post">' +
					  '<input type="hidden" name="uuid" value="' + uuid + '"/>' +
					  '<input type="hidden" name="_token" value="' + csrf + '"/>' +
					  '</form>');
					$('body').append(form);
					form.submit();
				}else if(event.data == YT.PlayerState.PLAYING){
					if(!interval){
						interval = window.setInterval(function(){
								updateBar(player.getCurrentTime(), player.getDuration());
						}, 300);
					}
				}
		}

		var bar = new ProgressBar.Line('#container', {
		  strokeWidth: 4,
		  easing: 'easeInOut',
		  duration: 0,
		  color: '#FFEA82',
		  trailColor: '#eee',
		  trailWidth: 0,
		  svgStyle: {width: '100%', height: '100%'}
		});
		bar.animate(0);  // Value from 0.0 to 1.0

		function updateBar(time, duration){
			var progress = (time * 100) / duration;
			progress = progress/100;
			bar.animate(progress);
		}
</script>
@endsection