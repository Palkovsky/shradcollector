<?php

use App\User;
use App\Shrad;
use SammyK\LaravelFacebookSdk\LaravelFacebookSdk;

//Static
/*
Route::get('/info', ['as' => 'home', function(){
	if(Auth::user())
    	return Redirect::action('ShradsController@index');
	$fb = app(LaravelFacebookSdk::class);
    $login_url = $fb->getLoginUrl(['email']);
	return view('welcome', ['login_url' => $login_url]);
}]);
*/


//Shrads
Route::get('/', ['middleware' => 'auth', 'uses' => 'ShradsController@index']);
Route::get('/element/{slug}', ['middleware' => 'auth', 'uses' => 'ShradsController@show']);
Route::get('/collect', ['middleware' => 'auth', 'uses' => 'ShradsController@present']);
Route::post('/collect', 'ShradsController@collect');

//Sessions
Route::get('/login', 'SessionsController@create');
Route::post('/dummy', 'SessionsController@dummy');
Route::post('/login', 'SessionsController@store');
Route::get('/logout', ['middleware' => 'web', 'uses' => 'SessionsController@destroy']);

//Users
#Route::get('/register', 'UsersController@create');
#Route::get('/profile', 'UsersController@show');
#Route::post('/register', 'UsersController@store');
Route::get('/facebook/callback', 'UsersController@facebook');

//Stats
Route::get('/ranking', function(){
	return view('stats', ['users' => User::all()->sortByDesc(function($user){
		return $user->score();
	}), 'shrads' => Shrad::all()]);
});