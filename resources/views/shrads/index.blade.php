@extends('layouts.default')

@section('title')
	Odłamki
@endsection

@section('content')

		<!-- The Modal -->
		@if($show_modal)
			<div id="id01" class="w3-modal w3-animate-opacity">
				<div class="w3-modal-content w3-animate-top" style="margin-top: 100px;">
					<div class="w3-container w3-blue"> 
					<span onclick="document.getElementById('id01').style.display='none'" class="w3-closebtn" style="margin: 7px;">&times;</span>
						<h4>Brawo!</h4>
					</div>
					<div class="w3-container w3-center w3-text-grey">
						<p>Udało Ci się zebrać wszystkie elementy. Możesz zgłosić się po odebranie nagrody.</p>
						<p>Twoje ID to:
							<h1>{{ Auth::user()-> id }}</h1>
						</p>
					</div>
				</div>
			</div>
		@endif
		
		<div class="w3-container w3-center w3-blue">
			<h3>Twoja układanka</h3>
		</div>
		<div class="w3-progress-container" style="height: 5px;">
			<div id="myBar" class="w3-progressbar w3-amber" style="width:1%;"></div>
		</div>

		@foreach($shrads as $index => $shrad)
			@if ($index%3==0)
				@if ($index == 0)
					<div class="w3-container w3-center w3-animate-opacity" style="margin-top: 70px;">
				@else
					</div>
					<div class="w3-container w3-center w3-animate-opacity" style="margin-top: -4px;">
				@endif
			@endif
			
			@if (Auth::user()->isOwner($shrad))
				<a href="{{ action('ShradsController@show', $shrad->slug) }}">
					<img src="{{ $shrad->image_url }}" class="w3-animate-top" style="width: 100px; margin: -2px;"/>
				</a>
			@else
				<img src="{{ asset('images/picture.png')}}" class="w3-animate-top" style="width: 100px; margin: -2px;"/>
			@endif
		@endforeach
		<div class="w3-container w3-center w3-small w3-text-grey">
			<p>Aby zobaczyć film jeszcze raz, kliknij w element</p>
		</div>


		@if(Auth::user()->shrads()->get()->count() == $shrads->count())
			<div class="w3-container w3-center w3-large w3-text-grey" style="margin-top: 70px; margin-bottom: 50px;">
				<p>Zebrałeś wszystkie elementy! <br/>
				Twoje ID to: <strong>{{ Auth::user()->id }}</strong><p/>
				<iframe width="560" height="315" src="https://www.youtube.com/embed/rW1_8kLHUxw" frameborder="0" allowfullscreen></iframe>
			</div>
		@else
			<div class="w3-container w3-center w3-large w3-text-grey" style="margin-top: 70px; margin-bottom: 50px;">
				<p>Brakuje Ci jeszcze {{ $pending_shrads }} elementów</p>
			</div>
		@endif

	<script>
		function move() {
			var elem = document.getElementById("myBar"); 
			var width = 1;
			var id = setInterval(frame, 10);
			function frame() {
				if (width >= <?php echo($percentage); ?>) {
					clearInterval(id);
				} else {
					width++; 
					elem.style.width = width + '%'; 
				}
			}
		}
		function modal() {
			document.getElementById('id01').style.display='block';
		}	
		setTimeout('move()', 200);
		setTimeout('modal()', 1100);
	</script>
@endsection