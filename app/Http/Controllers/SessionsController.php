<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Laracasts\Flash\Flash;
use SammyK\LaravelFacebookSdk\LaravelFacebookSdk;

use App\Http\Requests;
use App\User;
use Faker\Factory as Faker;

class SessionsController extends Controller
{
    
    public function create(){
        if(Auth::user())
            return Redirect::action('ShradsController@index');

        $fb = app(LaravelFacebookSdk::class);
        $login_url = $fb->getLoginUrl(['email']);
    	return view('sessions/create', ['login_url' => $login_url]);
    }

    public function dummy(){
        if(Auth::user())
            return Redirect::action('ShradsController@index');

        $user = new User;

        $faker = Faker::create(); 
        $user->name = $faker->userName;

        $user->email = $faker->email;
        $user->password = bcrypt('');
        $user->save();

        Auth::login($user);  
        return Redirect::intended('/');
    }

    public function store(){
    	$input = Input::all();

    	$attempt = Auth::attempt([
    			'email' => $input['email'],
    			'password' => $input['hasło']
    		]);

    	if ($attempt){
    	 	return Redirect::intended('/');
    	}

        if(User::where('email' , '=', $input['email'])->first()){
        	return Redirect::back()
        		->withErrors(["Niepoprawna nazwa użytkownika lub hasło"])
        		->withInput(Input::except('password'));
        }else{
            $rules = array(
                'email' => 'required|email|unique:users',
                'hasło' => 'required'
            );
            $validator = Validator::make($input, $rules);
            
            if ($validator->fails()) {
                return Redirect::action('SessionsController@create')
                    ->withErrors($validator)
                    ->withInput(Input::except('hasło'));
            }else{
                $user = new User;

                $faker = Faker::create(); 
                $user->name = $faker->userName;

                $user->email = $input['email'];
                $user->password = bcrypt($input['hasło']);
                $user->save();

                Auth::login($user);  
                return Redirect::intended('/');
            }
    }
}

    public function destroy(){
    	Auth::logout();
    	Flash::message("Wylogowano!");
    	return Redirect::action('SessionsController@create');
    }
}
